#include "multiplatform.hpp"
#include "update.hpp"

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>

float getCurrentVersion() {
    std::string str = getStarMadePath();
    str += ".StarMade/dist/current/version.txt";

    std::ifstream file(str.c_str(), std::ios_base::in);
    //std::FILE *fp;
    //fp = std::fopen(str, "rb");

    if (0 == std::getline(file, str)) {
        std::cerr << "ni mo\n";
        return 0.0f;
    }

    std::cerr << str << std::endl;
    return 1.0f;

}
