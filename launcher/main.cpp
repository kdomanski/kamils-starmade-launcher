#include "mainwindow.h"
#include "multiplatform.hpp"
#include "update.hpp"
#include <QApplication>

int main(int argc, char *argv[])
{
    getCurrentVersion();
    getStarMadePath();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
