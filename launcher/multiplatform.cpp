#include "multiplatform.hpp"

#ifdef __gnu_linux__

#include <unistd.h>
#include <pwd.h>

std::string getStarMadePath() {
    struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
    std::string str(pw->pw_dir);
    str += "/.StarMade/";
    return str;
}

#elif _WIN32

std::string getStarMadePath() {
    std::cerr << "WINDOWS BEECH!\n";
    return std::string();
}

#endif
