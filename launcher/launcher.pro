#-------------------------------------------------
#
# Project created by QtCreator 2013-10-23T15:48:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = launcher
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    multiplatform.cpp \
    update.cpp

HEADERS  += mainwindow.h \
    multiplatform.hpp \
    update.hpp
