#include <QApplication>
#include <QDesktopWidget>
#include <QToolBox>

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle("KSM Launcher v1");
    setGeometry(QRect(0, 0, 640, 480));
    move(QApplication::desktop()->screen()->rect().center() - rect().center());

    QWidget *page1 = new QWidget();
    QWidget *page2 = new QWidget();


    QToolBox *toolbox = new QToolBox(this);
    toolbox->setGeometry(440, 0, 200, 480);
    toolbox->addItem(page1, "Launch");
    toolbox->addItem(page2, "Advanced");
}

MainWindow::~MainWindow()
{
    /*delete page1;
    delete page2;
    delete toolbox;*/
}
